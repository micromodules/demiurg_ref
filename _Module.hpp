#include "../DModuleCore.hpp"
#include "demiurg_typescore/_Module.hpp"
#include "demiurg_compiletime_reflection/_Module.hpp"

#if DMACRO_CheckModuleVersion(demiurg_typescore, 1, 1) &&\
    DMACRO_CheckModuleVersion(demiurg_compiletime_reflection, 1, 1)
#define MODULE__demiurg_ref__Version 1
#endif
