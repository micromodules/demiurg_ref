#pragma once
#include "../_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "DDebugLevels.hpp"
#include "demiurg_typescore/IntegerTypes.hpp"
#include "demiurg_typescore/PointerTypes.hpp"
#include "demiurg_diagnostic/Check.hpp"
#include "demiurg_diagnostic/Exception.hpp"

#include <vector>//For DMapAsArrayOfPairs
#include <algorithm>
#include <cstddef>//For size_t
#include <utility>//For std::move(...)
#include <iostream>

//===========================================================[DMapAsArrayOfPairs]===========================================================

namespace Check {
    struct MapAsArrayReturningWithNoElementException : public Exception{};
}

template<typename T_Key, typename T_Value>
class DMapAsArrayOfPairs { };

template<typename T_KeyPointerType, typename T_Value>
class DMapAsArrayOfPairs<T_KeyPointerType*, T_Value> {
public:
    T_Value& getOrCreateIfNo(T_KeyPointerType* inKey, T_Value&& inValueToMove) {
        for (Element& theElement : _elements)
            if (theElement.key == inKey)
                return theElement.value;

        _elements.push_back(Element(inKey, std::move(inValueToMove)));
        return _elements.back().value;
    }

    T_Value* get(T_KeyPointerType* inKey) {
        for (const Element& theElement : _elements)
            if (theElement.key == inKey)
                return theElement.value;
        return nullptr;
    }

    const T_Value* get(T_KeyPointerType* inKey) const {
        for (const Element& theElement : _elements)
            if (theElement.key == inKey)
                return theElement.value;
        return nullptr;
    }

    void remove(T_KeyPointerType* inKey) {
        auto theIterator = _elements.begin();
        for (; theIterator != _elements.end() && inKey != theIterator->key; ++theIterator);
        if (theIterator != _elements.end())
            _elements.erase(theIterator);
    }

    T_Value removeReturning(T_KeyPointerType* inKey) {
        auto theIterator = _elements.begin();
        for (; theIterator != _elements.end() && inKey != theIterator->key; ++theIterator);
        DCheck(theIterator != _elements.end(), Check::MapAsArrayReturningWithNoElementException{ });

        T_Value theResult{ std::move(theIterator->value) };
        _elements.erase(theIterator);
        return theResult;
    }

    size_t isEmpty() const { return (0 == getElementsNum()); }
    size_t getElementsNum() const { return _elements.size(); }

    template<typename T_IterationFunctorType>
    void iterate(const T_IterationFunctorType& inIterationFunctor) {
        for(Element& theElement : _elements)
            inIterationFunctor(theElement.key, theElement.value);
    }

    template<typename T_IterationFunctorType>
    void iterate(const T_IterationFunctorType& inIterationFunctor) const {
        for(const Element& theElement : _elements)
            inIterationFunctor(theElement.key, theElement.value);
    }

private:
    struct Element {
        Element(Element&& inElement) noexcept
            : key(inElement.key), value(std::move(inElement.value)) { }

        Element(T_KeyPointerType* inKey, T_Value&& inValue)
            : key(inKey), value(std::move(inValue)) { }

        Element& operator=(Element&& inOtherElement) {
            key = inOtherElement.key;
            value = std::move(inOtherElement.value);

            return *this;
        }

        T_KeyPointerType* key = nullptr;
        T_Value value;
    };

    std::vector<Element> _elements;
};

//===========================================================[DPlaceInSourceInfo]===========================================================

#define TO_STRING_HELPER(M_ExpandedValue) #M_ExpandedValue
#define TO_STRING(M_Value) TO_STRING_HELPER(M_Value)

struct DPlaceInSourceInfo
{
    //NB: Object context is not protected from dangling pointers. Stores useful info only until object was not destructed
    template<bool Empty = true> //NB: Empty template to prevent weak-vtables warnings (force allowed inline vtable declaration)
    struct IObjectInfo {
        //NB: To supporty polymorphic class view in debugger class should have vtable.
        // We add Destructor to force vtable emitting for class
        virtual ~IObjectInfo()=default;
    };
    template<typename T_ObjectType> struct ObjectInfo : IObjectInfo<> {
        ObjectInfo(T_ObjectType* inObject) : object(inObject) {}
        T_ObjectType* object = nullptr;
    };

    DPlaceInSourceInfo(const DPlaceInSourceInfo& inOtherInfo) = delete;

    DPlaceInSourceInfo()
        : functionName("<unspecified>"), fileName("<unspecified>"), line(0), objectInfo(nullptr) { }

    template<typename T_ObjectType>
    DPlaceInSourceInfo(const char* inFunctionName, const char* inFileName, const IntegerType<4, false> inLine, T_ObjectType* inContextObject)
        : functionName(inFunctionName), fileName(inFileName), line(inLine), objectInfo(new ObjectInfo<T_ObjectType>(inContextObject)) { }

    DPlaceInSourceInfo(const char* inFunctionName, const char* inFileName, const IntegerType<4, false> inLine)
        : functionName(inFunctionName), fileName(inFileName), line(inLine), objectInfo(nullptr) { }

    DPlaceInSourceInfo(DPlaceInSourceInfo&& inOtherInfo) noexcept
        : functionName(inOtherInfo.functionName),
          fileName(inOtherInfo.fileName),
          line(inOtherInfo.line),
          objectInfo(inOtherInfo.objectInfo)
    {
        inOtherInfo.objectInfo = nullptr;
    }

    DPlaceInSourceInfo& operator=(DPlaceInSourceInfo&& inOtherInfo) {
        functionName = inOtherInfo.functionName;
        fileName = inOtherInfo.fileName;
        line = inOtherInfo.line;
        objectInfo = inOtherInfo.objectInfo;

        inOtherInfo.objectInfo = nullptr;

        return *this;
    }

    ~DPlaceInSourceInfo() {
        if (nullptr != objectInfo)
            delete objectInfo;
    }

    const char* functionName = nullptr;
    const char* fileName = nullptr;
    IntegerType<4, false> line = 0;
    IObjectInfo<>* objectInfo = nullptr;
};

#define context_file __FILE__
#define context_line __LINE__
#define context_callFullStingInfo __func__ " (" context_file ":" TO_STRING(context_line) ")"

#define context() DPlaceInSourceInfo{ __func__, __FILE__, __LINE__, this }

#define contextNoClass() DPlaceInSourceInfo{ __func__, __FILE__, __LINE__ }

//======================================================[DRefDebugData]=====================================================================

namespace Check {
    struct NotNullableRefsLeftOnObjectDestroyException : Exception{ };
    struct DoubleElementRefister : Exception{ };
    struct RemovingOfUnregisteredRef : Exception{ };
}

struct DRefDebugData {
    void performCheckOnObjectDestroy() const {
        if (0 != referecnesThatPointsObject.size())
            DCheck(false, Check::NotNullableRefsLeftOnObjectDestroyException{ });
    }

    void registerRefForDebug(DByteType* inRefPointer) {
        //TODO: Idea for finding context, in where ref exists:
        //1. Check if reference adress is in any storage range. If such storage found - find object
        // in storage, where ref is placed and return object + byte offset in object (and, possible, it's
        // possible event to find field if use clang source analysing with computing offset).
        //2. If memory is on stack - use stack dump to find function's stack range (arguments+local variables).
        //3. Otherwise (for example, placed in collection that allocates it's data buffer on heap)
        // I CURRENTLY DONT KNOW WHAT TO DO... :(
        const auto theEnd = referecnesThatPointsObject.end();
        DCheck(theEnd == std::find(referecnesThatPointsObject.begin(), theEnd, inRefPointer), Check::DoubleElementRefister{ });
        referecnesThatPointsObject.push_back(inRefPointer);
   }

    void unregisterRefForDebug(DByteType* inRefPointer) {
        const auto theEnd = referecnesThatPointsObject.end();
        const auto theIterator = std::find(referecnesThatPointsObject.begin(), theEnd, inRefPointer);
        DCheck(theIterator != theEnd, Check::RemovingOfUnregisteredRef{ });
        referecnesThatPointsObject.erase(theIterator);
    }

    std::vector<DByteType*> referecnesThatPointsObject;
};

//==========================================================[Debug macro]===================================================================

#ifdef DRefDebug_CheckDestroyForNotNullableRefs
#define refInit(M_RefExpression) M_RefExpression, context()
#define refInitNoClass(M_RefExpression) M_RefExpression, contextNoClass()
#else
#define refInit(M_RefExpression) {M_RefExpression}
#define refInitNoClass(M_RefExpression) {M_RefExpression}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
