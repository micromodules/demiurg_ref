#pragma once
#include "../_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

//=======================================================[Debug level declaration]==========================================================

#define DRefDebug_Level 2

//=========================================================[Level declarations]=============================================================

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[Level 1]

#if DRefDebug_Level > 0
#   define DRefDebug_CheckDestroyForNotNullableRefs
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[Level 2]

#if DRefDebug_Level > 1
#endif

#endif
