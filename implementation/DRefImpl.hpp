#pragma once
#include "../_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "demiurg_ref/DRefAttributes.hpp"
#include "demiurg_ref/implementation/DStorageImpl.hpp"

#include "demiurg_diagnostic/Unused.hpp"

template<class T_Type, class T_Attributes>
class DRef;

//NB: Struct for possibility to friend all functions here
struct DRef_Impl
{
    template<typename T_LeftType, typename T_LeftAPIMut, typename T_LeftRefMut,
            typename T_RightType, typename T_RightAPIMut, typename T_RightRefMut>
    static bool isEquals(
            const DRef<T_LeftType, DRefAttributes<T_LeftAPIMut, T_LeftRefMut, Nullable>>& inLeftRef,
            const DRef<T_RightType, DRefAttributes<T_RightAPIMut, T_RightRefMut, Nullable>>& inRightRef)
    {
        if (inLeftRef.isValid() && inRightRef.isValid()) {
            const StorageBase::SEUD& theLeftRefSEUD = inLeftRef._getSEUD();
            const StorageBase::SEUD& theRightRefSEUD = inRightRef._getSEUD();
            return (&theLeftRefSEUD == &theRightRefSEUD) && (theLeftRefSEUD.SEGID == theRightRefSEUD.SEGID);
        } else {
            return false;
        }
    }

    template<typename T_LeftType, typename T_LeftAPIMut, typename T_LeftRefMut,
            typename T_RightType, typename T_RightAPIMut, typename T_RightRefMut>
    static bool isEquals(
            const DRef<T_LeftType, DRefAttributes<T_LeftAPIMut, T_LeftRefMut, Nullable>>& inLeftRef,
            const DRef<T_RightType, DRefAttributes<T_RightAPIMut, T_RightRefMut, NotNullable>>& inRightRef)
    {
        return inLeftRef.isValid() && (&inLeftRef._getSEUD() == &inRightRef._getSEUD());
    }

    template<typename T_LeftType, typename T_LeftAPIMut, typename T_LeftRefMut,
            typename T_RightType, typename T_RightAPIMut, typename T_RightRefMut>
    static bool isEquals(
            const DRef<T_LeftType, DRefAttributes<T_LeftAPIMut, T_LeftRefMut, NotNullable>>& inLeftRef,
            const DRef<T_RightType, DRefAttributes<T_RightAPIMut, T_RightRefMut, Nullable>>& inRightRef)
    {
        return isEquals(inRightRef, inLeftRef);
    }

    template<typename T_LeftType, typename T_LeftAPIMut, typename T_LeftRefMut,
            typename T_RightType, typename T_RightAPIMut, typename T_RightRefMut>
    static bool isEquals(
            const DRef<T_LeftType, DRefAttributes<T_LeftAPIMut, T_LeftRefMut, NotNullable>>& inLeftRef,
            const DRef<T_RightType, DRefAttributes<T_RightAPIMut, T_RightRefMut, NotNullable>>& inRightRef)
    {
        return (&inLeftRef._getSEUD() == &inRightRef._getSEUD());
    }

    // -------------------------------------------------------------------------------------------------------

    template<typename T_TypeA, typename T_TypeB>
    static bool isEquals(const DOwningRef<T_TypeA>& inRefA, const DOwningRef<T_TypeB>& inRefB) {
        if (!inRefA.isValid() || !inRefB.isValid()) return false;
        return (&inRefA._getSEUD() == &inRefB._getSEUD());
    }

    template<typename T_OwningRefType, typename T_RefType, typename T_RefAPIMut, typename T_RefRefMut>
    static bool isEquals(
            const DOwningRef<T_OwningRefType>& inOwningRef,
            const DRef<T_RefType, DRefAttributes<T_RefAPIMut, T_RefRefMut, Nullable>>& inRef)
    {
        if (!inOwningRef.isValid() || !inRef.isValid()) return false;
        const StorageBase::SEUD& theOwningRefSEUD = inOwningRef._getSEUD();
        const StorageBase::SEUD& theRefSEUD = inRef._getSEUD();
        return (&theOwningRefSEUD == &theRefSEUD) && (theOwningRefSEUD.SEGID == theRefSEUD.SEGID);
    }

    template<typename T_OwningRefType, typename T_RefType, typename T_RefAPIMut, typename T_RefRefMut>
    static bool isEquals(
            const DOwningRef<T_OwningRefType>& inOwningRef,
            const DRef<T_RefType, DRefAttributes<T_RefAPIMut, T_RefRefMut, NotNullable>>& inRef)
    {
        if (!inOwningRef.isValid()) return false;
        return (&inOwningRef._getSEUD() == &inRef._getSEUD());
    }
};

// =================================================================

template<typename T_Type> class DOwningRef;

template<typename T_Type, typename T_Attributes> class DRef;

template<typename T_Type, typename T_APIMut, typename T_RefMut, typename T_Nullability>
bool isRefValid(const DRef<T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&);

template<typename T_Type>
bool isRefValid(const DOwningRef<T_Type>& inRef) { return inRef.isValid(); }

template<typename T_Type, typename T_APIMut, typename T_RefMut>
bool isRefValid(const DRef<T_Type, DRefAttributes<T_APIMut, T_RefMut, Nullable>>& inRef) { return inRef.isValid(); }

template<typename T_Type, typename T_APIMut, typename T_RefMut>
bool isRefValid(const DRef<T_Type, DRefAttributes<T_APIMut, T_RefMut, NotNullable>>& inRef) { DUnused(inRef); return true; }

// =================================================================

template<typename T_LeftType, typename T_LeftAPIMut, typename T_LeftRefMut, typename T_LeftNullability,
         typename T_RightType, typename T_RightAPIMut, typename T_RightRefMut, typename T_RightNullability>
bool operator==(
        const DRef<T_LeftType, DRefAttributes<T_LeftAPIMut, T_LeftRefMut, T_LeftNullability>>& inLeftRef,
        const DRef<T_RightType, DRefAttributes<T_RightAPIMut, T_RightRefMut, T_RightNullability>>& inRightRef)
{
    //DTestableStaticAssert((areRelatedClasses<T_LeftType, T_RightType>(), "", Check::InconsitanceTypesComparationException{});
    return DRef_Impl::isEquals(inLeftRef, inRightRef);
}

template<typename T_OwningRefType, typename T_RefType, typename T_RefAttributes>
bool operator==(const DOwningRef<T_OwningRefType>& inOwningRef, const DRef<T_RefType, T_RefAttributes>& inRef) {
    return DRef_Impl::isEquals(inOwningRef, inRef);
}

template<typename T_OwningRefType, typename T_RefType, typename T_RefAttributes>
bool operator==(const DRef<T_RefType, T_RefAttributes>& inRef, const DOwningRef<T_OwningRefType>& inOwningRef) {
    return DRef_Impl::isEquals(inOwningRef, inRef);
}

#endif
