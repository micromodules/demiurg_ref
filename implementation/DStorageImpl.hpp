#pragma once
#include "../_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "demiurg_typescore/IntegerTypes.hpp"
#include "demiurg_typescore/PointerTypes.hpp"

using SEGID_t = IntegerType<4, false>;
template<bool T_Signed> using SEUDOffset_t = IntegerType<4, T_Signed>;

// =================================================================

inline SEUDOffset_t<false> getSEUDOffset(
        const DByteType* inCOPFrom, const SEUDOffset_t<false> inSEUDOffsetFrom, const DByteType* inCOPTo)
{
    const SEUDOffset_t<true> theDelta = asRaw(inCOPFrom) - asRaw(inCOPTo);
    return static_cast<SEUDOffset_t<false>>(inSEUDOffsetFrom + theDelta);
}

inline SEUDOffset_t<false> getSEUDOffset(
        DByteType* inCOPFrom, const SEUDOffset_t<false> inSEUDOffsetFrom, DByteType* inCOPTo)
{
    const SEUDOffset_t<true> theDelta = asRaw(inCOPFrom) - asRaw(inCOPTo);
    return static_cast<SEUDOffset_t<false>>(inSEUDOffsetFrom + theDelta);
}

// =================================================================

class DStorageBase;

namespace StorageBase
{
    struct SEUD {
        SEUD(DStorageBase* inStoragePointer) : storagePointer(inStoragePointer) { }

        SEGID_t SEGID = 0;
        DStorageBase* storagePointer = nullptr;
#       ifdef DRefDebug_CheckDestroyForNotNullableRefs//{
        DRefDebugData referencesDebugData{ };
#       endif
    };
}

#endif//MODULE__demiurg_ref__Version
