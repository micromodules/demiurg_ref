#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "demiurg_typescore/TypesPack.hpp"

#include <type_traits>

//==========================================================================================================================================

//TODO: Use enums for attributes

struct DRefAPIMutabilityPolicy {};
struct MutableAPI : DRefAPIMutabilityPolicy {};
struct ConstAPI : DRefAPIMutabilityPolicy {};

struct DRefRefMutabilityPolicy {};
struct MutableRef : DRefRefMutabilityPolicy {};
struct ConstRef : DRefRefMutabilityPolicy {};

struct DRefNullabilityPolicy {};
struct Nullable : DRefNullabilityPolicy {};
struct NotNullable : DRefNullabilityPolicy {};

//==========================================================================================================================================

template<typename T_APIMutability, typename T_RefMutability, typename T_Nullability>
struct DRefAttributes
{
    template<typename TFirst, typename TSecond>
    static constexpr bool isSameType() { return std::is_same<TFirst, TSecond>::value; }

    static_assert(isSameType<T_APIMutability, MutableAPI>() || isSameType<T_APIMutability, ConstAPI>(), "Incorrect API Mutability");
    static_assert(isSameType<T_RefMutability, MutableRef>() || isSameType<T_RefMutability, ConstRef>(), "Incorrect Ref Mutability");
    static_assert(isSameType<T_Nullability, Nullable>() || isSameType<T_Nullability, NotNullable>(), "Incorrect nullability");

    using APIMutability = T_APIMutability;
    using RefMutability = T_RefMutability;
    using Nullability = T_Nullability;

    static constexpr bool isMutableAPI() { return isSameType<APIMutability, MutableAPI>(); }
    static constexpr bool isConstAPI() { return isSameType<APIMutability, ConstAPI>(); }

    static constexpr bool isMutableRef() { return isSameType<RefMutability, MutableRef>(); }
    static constexpr bool isConstRef() { return isSameType<RefMutability, ConstRef>(); }

    static constexpr bool isNullable() { return isSameType<Nullability, Nullable>(); }
    static constexpr bool isNotNullable() { return isSameType<Nullability, NotNullable>(); }
};

using MMN = DRefAttributes<  MutableAPI, MutableRef, Nullable     >;
using CMN = DRefAttributes<  ConstAPI,   MutableRef, Nullable     >;
using MCN = DRefAttributes<  MutableAPI, ConstRef,   Nullable     >;
using CCN = DRefAttributes<  ConstAPI,   ConstRef,   Nullable     >;

using CC =  DRefAttributes<  ConstAPI,   ConstRef,   NotNullable  >;
using MC =  DRefAttributes<  MutableAPI, ConstRef,   NotNullable  >;
using CM =  DRefAttributes<  ConstAPI,   MutableRef, NotNullable  >;
using MM =  DRefAttributes<  MutableAPI, MutableRef, NotNullable  >;

#endif
