#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "demiurg_typescore/Where.hpp"
#include "demiurg_typescore/InheritanceInfo.hpp"

template<typename T_Type_, typename T_Attributes_> class DRef;

template<typename T_Type>
class DOwningRef
{
public:
    typedef DOwningRef<T_Type> OwnType;

    DOwningRef()
        : _COP(nullptr),
          _SEUDOffset(sizeof(T_Type)){ }

    DOwningRef(OwnType&& inOtherRef) noexcept
        : _COP(inOtherRef._COP),
          _SEUDOffset(inOtherRef._SEUDOffset)
    {
        inOtherRef.invalidate();
    }

    template<typename _T_Type, Where(isAAssignableFromB<T_Type, _T_Type>())>
    DOwningRef(DOwningRef<_T_Type>&& inOtherRef) noexcept
        : _COP(static_cast<T_Type*>(Fit<_T_Type*>::_(inOtherRef._COP))),
          _SEUDOffset(getSEUDOffsetCOPCasting(asRaw(inOtherRef._COP), inOtherRef._SEUDOffset, asRaw(_COP)))
    {
        inOtherRef.invalidate();
    }

    template<typename T_OtherType>
    OwnType& operator=(DOwningRef<T_OtherType>&& inOtherRef) {
        free();
        _COP = inOtherRef._COP;
        _SEUDOffset = inOtherRef._SEUDOffset;
        inOtherRef.invalidate();
        return *this;
    }

    bool isValid() const { return (nullptr != _COP); }
    T_Type* operator->() const { DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ }); return _COP; }
    T_Type& operator*() const { DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ }); return _COP; }

    void free() { if (isValid()) destroyObject(); }
    ~DOwningRef() { if (isValid()) destroyObject(); }

private:
    friend struct DRef_Impl;
    template<typename T_Type_> friend struct DStorageHolder;
    template<typename T_TypeTo, typename T_TypeFrom> friend DOwningRef<T_TypeTo> DDynamicCast(DOwningRef<T_TypeFrom>&& inRefFrom);
    template<typename T_OtherType> friend class DOwningRef;

    DOwningRef(DByteType* inCOP, const SEUDOffset_t<false> inSizeOfObject)
        : DOwningRef(rawAsPtr<T_Type>(inCOP), inSizeOfObject) { new(_COP)T_Type(); }

    DOwningRef(T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset)
        : _COP(inCOP), _SEUDOffset(inSEUDOffset) { }

    StorageBase::SEUD& _getSEUD() { return const_cast<StorageBase::SEUD&>(const_cast<const OwnType*>(this)->_getSEUD()); }

    template<typename T_Type_, typename T_Attributes_> friend class DRef;
    const StorageBase::SEUD& _getSEUD() const { return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset); }
    SEUDOffset_t<false> _getSEUDOffset() const { return _SEUDOffset; }
    SEGID_t _getSEGID() const { return _getSEUD().SEGID; }

    void destroyObject() {
        DCheck(isValid(), Check::DestroyingOfDestroyedObjectException{ });

        _COP->~T_Type();

        StorageBase::SEUD& theSEUD = _getSEUD();

#       ifdef DRefDebug_CheckDestroyForNotNullableRefs//{
        theSEUD.referencesDebugData.performCheckOnObjectDestroy();
#       endif

        theSEUD.storagePointer->destroy(theSEUD);

        invalidate();
    }

    void invalidate() { _COP = nullptr; }

    T_Type* _COP = nullptr;
    SEUDOffset_t<false> _SEUDOffset = sizeof(T_Type);
};

#endif
