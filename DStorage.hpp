#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "implementation/DCheckDestroyForNotNullableRefsTypes.hpp"
#include "demiurg_diagnostic/TestableStaticAsserts.hpp"
#include "demiurg_diagnostic/Exception.hpp"
#include "DRefAttributes.hpp"
#include "implementation/DDebugLevels.hpp"
#include "implementation/DStorageImpl.hpp"

namespace Check
{
    struct DestroyingOfDestroyedObjectException : Exception {};
    struct DereferencingOfInvalidObjectException : Exception {};
    struct SetNullForNotNullableRefException : Exception {};
    struct ValidationCheckWithNullptrCOPException : Exception {};
    struct InconsitanceTypesComparationException : Exception {};
}

//Terms:
//-SEUD = Storage Element Utility Data
//-COP = Casted Object Pointer
//-SEGID = Storage Element Generation ID
//
//-Storage object = memory used for storing object data
//-Utility data = memory used for storing utility data for managing object
//-Storage element = "Storage object" + "Utility data"
//-Storage element reference = struct for accessing "Storage object"
class DStorageBase {
public:
    struct SEDataAccess {
        static void initSE(DByteType* inElementPtr, DStorageBase* inStorage, DByteType* inNextFreeElementPtr) {
            rawAsRef<DByteType*>(inElementPtr) = inNextFreeElementPtr;
            StorageBase::SEUD& theSEUD = SEDataAccess::getSEUDBy(inElementPtr, inStorage->_objectSize);
            new(&theSEUD)StorageBase::SEUD(inStorage);
        }

        static DMemorySizeType getByteSizeOfSE(const SEUDOffset_t<false> inObjectSize) {
            return inObjectSize + sizeof(StorageBase::SEUD);
        }

        static StorageBase::SEUD& getSEUDBy(DByteType* inCOP, const SEUDOffset_t<false> inSEUDOffset) {
            return rawAsRef<StorageBase::SEUD>(inCOP + inSEUDOffset);
        }

        static const StorageBase::SEUD& getSEUDBy(const DByteType* inCOP, const SEUDOffset_t<false> inSEUDOffset) {
            return rawAsRef<StorageBase::SEUD>(inCOP + inSEUDOffset);
        }

        static bool isRefValidBy(const DByteType* inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID) {
            //TODO: Validate using generation ID only to prevent branching
            if (nullptr == inCOP) return false;
            const StorageBase::SEUD& theUtilityData = SEDataAccess::getSEUDBy(inCOP, inSEUDOffset);
            return (inSEGID == theUtilityData.SEGID);
        }

        static bool isRefValidBy(const DByteType& inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID) {
            const StorageBase::SEUD& theUtilityData = SEDataAccess::getSEUDBy(&inCOP, inSEUDOffset);
            return (inSEGID == theUtilityData.SEGID);
        }

        static DByteType* getSEAddressBy(const SEUDOffset_t<false> inObjectSize, StorageBase::SEUD& inSEUD) {
            return asRaw(inSEUD) - inObjectSize;
        }
    };

    DStorageBase(const SEUDOffset_t<false> inObjectSize)
        : _objectSize(inObjectSize),
          _batches(nullptr), _batchesNum(0), _topFreeElement(nullptr)
    {
        createInitialBatch();
    }

    DByteType* create() {
        reserveNewBatchIfNeed();

        DByteType* theResult = _topFreeElement;
        _topFreeElement = rawAsRef<DByteType*>(_topFreeElement);
        return theResult;
    }

    void destroy(StorageBase::SEUD& inSEUD) {
        //TODO: Add check for generation ID here (if there is no overflow & ignore element if overflow take place)
        ++inSEUD.SEGID;

        DByteType* theElementAddress = SEDataAccess::getSEAddressBy(_objectSize, inSEUD);
        rawAsRef<DByteType*>(theElementAddress) = _topFreeElement;
        _topFreeElement = theElementAddress;
    }

    void createInitialBatch() {
        _batchesNum = 1;
        _batches = new DByteType*[_batchesNum];

        DByteType* theNewBatch = allocateBatch();
        _batches[0] = theNewBatch;
        _topFreeElement = theNewBatch;
    }

    void reserveNewBatchIfNeed() {
        if (nullptr == _topFreeElement)
            reserveNewBatch();
    }

    void reserveNewBatch() {
        DByteType** theOldBatches = _batches;
        const DMemorySizeType theOldBatchesNum = _batchesNum;

        _batches = new DByteType*[++_batchesNum];
        memcpy(_batches, theOldBatches, getByteSizeOfBatch() * theOldBatchesNum);
        delete[] theOldBatches;

        DByteType* theNewBatch = allocateBatch();
        _batches[theOldBatchesNum] = theNewBatch;
        _topFreeElement = theNewBatch;
    }

    DByteType* allocateBatch() {
        DByteType* theBatchMemory = new DByteType[getByteSizeOfBatch()];

        const DMemorySizeType kByteSizeOfElement = getByteSizeOfElement();

        DByteType* theIteratingElementPointer = theBatchMemory;
        const DByteType* theLastElementPointer = theIteratingElementPointer + kByteSizeOfElement * (kBatchSize - 1);

        while (theIteratingElementPointer != theLastElementPointer) {
            SEDataAccess::initSE(theIteratingElementPointer, this, theIteratingElementPointer + kByteSizeOfElement);
            theIteratingElementPointer += kByteSizeOfElement;
        }
        SEDataAccess::initSE(theIteratingElementPointer, this, nullptr);

        return theBatchMemory;
    }

    DMemorySizeType getByteSizeOfElement() const { return SEDataAccess::getByteSizeOfSE(_objectSize); }
    DMemorySizeType getByteSizeOfBatch() const { return getByteSizeOfElement() * kBatchSize; }

    //Fields
    SEUDOffset_t<false> _objectSize;

    DByteType** _batches = nullptr;
    DMemorySizeType _batchesNum = 0;
    DByteType* _topFreeElement = nullptr;

    static const DMemorySizeType kBatchSize = 2;
};

#endif
