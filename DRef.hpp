#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "DStorage.hpp"
#include "DOwningRef.hpp"
#include "implementation/DRefImpl.hpp"
#include "implementation/DDebugLevels.hpp"

#include "demiurg_typescore/Where.hpp"
#include "demiurg_typescore/InheritanceInfo.hpp"

using namespace std;

template<class T_Type, class T_Attributes>
class DRef;

#ifdef DRefDebug_CheckDestroyForNotNullableRefs//{
#define registerReferenceForObjectDestroyChecks(M_Ref) M_Ref->_getSEUD().referencesDebugData.registerRefForDebug(asRaw(M_Ref))
#define unregisterReferenceForObjectDestroyChecks(M_Ref) M_Ref->_getSEUD().referencesDebugData.unregisterRefForDebug(asRaw(M_Ref));
#else
#define registerReferenceForObjectDestroyChecks(M_Ref)
#define unregisterReferenceForObjectDestroyChecks(M_Ref)
#endif//} DRefDebug_CheckDestroyForNotNullableRefs

#define DRef_AllowImplicitRValueToLValueCast false

// (1)---Default constructing
// (2)---Assigning & moving (constructing)
// (3)---Assigning & moving (assignment)
// (4)---Dereferencing
// (5)---Validity checking
// (6)---Invalidation
// (7)---Equality [as friend operator]

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DETAILS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

template<class T_Type, class T_Attributes>
class DRefDetails;

//=====================================================[ Nullable ]=========================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M M] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//TODO: Add support for scalar types wrapping

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(_T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID)
        : _COP(static_cast<T_Type*>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP))), _SEGID(inSEGID)
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");
    }

    template<class _T_Type>
    void _assign(_T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID) {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        _COP = static_cast<T_Type*>(inCOP);
        _SEUDOffset = getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP));
        _SEGID = inSEGID;
    }

    bool _isValid() const {
        return DStorageBase::SEDataAccess::isRefValidBy(asRaw(_COP), _SEUDOffset, _SEGID);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    T_Type* _getCOP() const { return _COP; }

    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }
    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _SEGID;
    }

    T_Type* _COP{ Fit<T_Type*>::_(nullptr) };
    SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
    SEGID_t _SEGID{ 0 };
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(const _T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID)
        : _COP(static_cast<const T_Type*>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP))), _SEGID(inSEGID)
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");
    }

    template<class _T_Type>
    void _assign(const _T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID) {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        _COP = static_cast<const T_Type*>(inCOP);
        _SEUDOffset = getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP));
        _SEGID = inSEGID;
    }

    bool _isValid() const {
        return DStorageBase::SEDataAccess::isRefValidBy(asRaw(_COP), _SEUDOffset, _SEGID);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    const T_Type* _getCOP() const { return _COP; }

    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }
    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _SEGID;
    }

    const T_Type* _COP{ Fit<const T_Type*>::_(nullptr) };
    SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
    SEGID_t _SEGID{ 0 };
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(_T_Type& inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID)
        : _COP(static_cast<T_Type&>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP))), _SEGID(inSEGID)
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");
    }

    bool _isValid() const {
        return DStorageBase::SEDataAccess::isRefValidBy(asRaw(_COP), _SEUDOffset, _SEGID);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    T_Type& _getCOP() const { return _COP; }

    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }
    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _SEGID;
    }

    T_Type& _COP{ Fit<T_Type&>::_(nullptr) };
    const SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
    const SEGID_t _SEGID{ 0 };
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(const _T_Type& inCOP, const SEUDOffset_t<false> inSEUDOffset, const SEGID_t inSEGID)
        : _COP(static_cast<const T_Type&>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP))), _SEGID(inSEGID)
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");
    }

    bool _isValid() const {
        return DStorageBase::SEDataAccess::isRefValidBy(asRaw(_COP), _SEUDOffset, _SEGID);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    const T_Type& _getCOP() const { return _COP; }

    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }
    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _SEGID;
    }

    const T_Type& _COP{ Fit<const T_Type&>::_(nullptr) };
    const SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
    const SEGID_t _SEGID{ 0 };
};

//===================================================[ Not Nullable ]=======================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(_T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset)
        : _COP(static_cast<T_Type*>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP)))
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        registerReferenceForObjectDestroyChecks(this);
    }

    template<class _T_Type>
    void _assign(_T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset) {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        unregisterReferenceForObjectDestroyChecks(this);
        _COP = static_cast<T_Type*>(inCOP);
        _SEUDOffset = getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP));
        registerReferenceForObjectDestroyChecks(this);
    }

    StorageBase::SEUD& _getSEUD() {
        using OwnType = DRefDetails<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>;
        return const_cast<StorageBase::SEUD&>(const_cast<const OwnType*>(this)->_getSEUD());
    }
    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    T_Type* _getCOP() const { return _COP; }

    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _getSEUD().SEGID;
    }

    T_Type* _COP{ nullptr };
    SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(const _T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset)
        : _COP(static_cast<const T_Type*>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP)))
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        registerReferenceForObjectDestroyChecks(this);
    }

    template<class _T_Type>
    void _assign(const _T_Type* inCOP, const SEUDOffset_t<false> inSEUDOffset) {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        registerReferenceForObjectDestroyChecks(this);
        _COP = static_cast<const T_Type*>(inCOP);
        _SEUDOffset = getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP));
        unregisterReferenceForObjectDestroyChecks(this);
    }

    StorageBase::SEUD& _getSEUD() {
        using OwnType = DRefDetails<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>;
        return const_cast<StorageBase::SEUD&>(const_cast<const OwnType*>(this)->_getSEUD());
    }
    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    const T_Type* _getCOP() const { return _COP; }

    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _getSEUD().SEGID;
    }

    const T_Type* _COP{ nullptr };
    SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(_T_Type& inCOP, const SEUDOffset_t<false> inSEUDOffset)
        : _COP(static_cast<T_Type&>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(&inCOP), inSEUDOffset, asRaw(&_COP)))
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        registerReferenceForObjectDestroyChecks(this);
    }

    StorageBase::SEUD& _getSEUD() {
        using OwnType = DRefDetails<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>;
        return const_cast<StorageBase::SEUD&>(const_cast<const OwnType*>(this)->_getSEUD());
    }
    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    T_Type& _getCOP() const { return _COP; }

    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _getSEUD().SEGID;
    }

    T_Type& _COP{ Fit<T_Type&>::_(nullptr) };
    const SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRefDetails<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>
{
protected:
    template<class _T_Type>
    DRefDetails(const _T_Type& inCOP, const SEUDOffset_t<false> inSEUDOffset)
        : _COP(static_cast<const T_Type&>(inCOP)), _SEUDOffset(getSEUDOffset(asRaw(inCOP), inSEUDOffset, asRaw(_COP)))
    {
        static_assert(isAAssignableFromB<T_Type*, _T_Type*>(), "Incorrect type in DRef details");

        registerReferenceForObjectDestroyChecks(this);
    }

    StorageBase::SEUD& _getSEUD() {
        using OwnType = DRefDetails<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>;
        return const_cast<StorageBase::SEUD&>(const_cast<const OwnType*>(this)->_getSEUD());
    }
    const StorageBase::SEUD& _getSEUD() const {
        return DStorageBase::SEDataAccess::getSEUDBy(asRaw(_COP), _SEUDOffset);
    }

    friend struct DRef_Impl;
    template<class _T_Type, class _T_Attributes> friend class DRef;

    const T_Type& _getCOP() const { return _COP; }

    SEUDOffset_t<false> _getSEUDOffset() const {
        return _SEUDOffset;
    }
    SEGID_t _getSEGID() const {
        return _getSEUD().SEGID;
    }

    const T_Type& _COP{ Fit<const T_Type&>::_(nullptr) };
    const SEUDOffset_t<false> _SEUDOffset{ sizeof(T_Type) };
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//=====================================================[ Nullable ]=========================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M M] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//TODO: Add support for scalar types wrapping

template<class T_Type>
class DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>
        : public DRefDetails<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>
{
public:
    using Attributes = DRefAttributes<MutableAPI, MutableRef, Nullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef();

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&& inRef);

    template<class _T_Type, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DOwningRef<_T_Type>& inRef);

    OwnType& operator=(const OwnType& inRef);
    OwnType& operator=(OwnType&& inRef);

    OwnType& operator=(int){ return *this; }

    template<class _T_Type, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (4)
    T_Type* operator->() const;
    T_Type& operator*() const;

    // (5)
    bool isValid() const;

    // (6)
    void invalidate();
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>
        : public DRefDetails<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>
{
public:
    using Attributes = DRefAttributes<ConstAPI, MutableRef, Nullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef();

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&& inRef);

    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, class T_APIMut, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DOwningRef<_T_Type>& inRef);

    OwnType& operator=(const OwnType& inRef);
    OwnType& operator=(OwnType&& inRef);

    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, class T_APIMut, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (4)
    const T_Type* operator->() const;
    const T_Type& operator*() const;

    // (5)
    bool isValid() const;

    // (6)
    void invalidate();
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>
        : public DRefDetails<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>
{
public:
    using Attributes = DRefAttributes<MutableAPI, ConstRef, Nullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef();

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&& inRef) = delete;//NB: Forbidden because of ConstRef

    template<class _T_Type, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3) NB: Forbidden at all because of ConstRef
    OwnType& operator=(const OwnType& inRef) = delete;
    OwnType& operator=(OwnType&& inRef) = delete;

    // (4)
    T_Type* operator->() const;
    T_Type& operator*() const;

    // (5)
    bool isValid() const;

    // (6) NB: Forbidden at all because of ConstRef
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>
        : public DRefDetails<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>
{
public:
    using Attributes = DRefAttributes<ConstAPI, ConstRef, Nullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef();

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&& inRef) = delete;//NB: Forbidden because of ConstRef

    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, typename T_APIMut, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3) NB: Forbidden at all because of ConstRef
    OwnType& operator=(const OwnType& inRef) = delete;
    OwnType& operator=(OwnType&& inRef) = delete;

    // (4)
    const T_Type* operator->() const;
    const T_Type& operator*() const;

    // (5)
    bool isValid() const;

    // (6) NB: Forbidden at all because of ConstRef
};

//===================================================[ Not Nullable ]=======================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>
        : public DRefDetails<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>
{
public:
    using Attributes = DRefAttributes<MutableAPI, MutableRef, NotNullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef() = delete;//NB: Forbidden because of NotNullable

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

    template<class _T_Type, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DOwningRef<_T_Type>& inRef);

    OwnType& operator=(const OwnType& inRef);
    OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden because of NotNullable

    template<class _T_Type, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (4)
    T_Type* operator->() const;
    T_Type& operator*() const;

    // (5,6) NB: No sense because of NotNullable

    // --- Implementation-specific
    ~DRef();
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>
        : public DRefDetails<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>
{
public:
    using Attributes = DRefAttributes<ConstAPI, MutableRef, NotNullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef() = delete;//NB: Forbidden because of NotNullable

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, class T_APIMut, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DOwningRef<_T_Type>& inRef);

    OwnType& operator=(const OwnType& inRef);
    OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden because of NotNullable

    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, class T_APIMut, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    OwnType& operator=(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (4)
    const T_Type* operator->() const;
    const T_Type& operator*() const;

    // (5,6) NB: No sense because of NotNullable

    // --- Implementation-specific
    ~DRef();
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>
        : public DRefDetails<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>
{
public:
    using Attributes = DRefAttributes<MutableAPI, ConstRef, NotNullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef() = delete;//NB: Forbidden because of NotNullable

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

    template<class _T_Type, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3) NB: Forbidden at all because of ConstRef
    OwnType& operator=(const OwnType& inRef) = delete;
    OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden because of NotNullable

    // (4)
    T_Type* operator->() const;
    T_Type& operator*() const;

    // (5,6) NB: No sense because of NotNullable

    // --- Implementation-specific
    ~DRef();
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

template<class T_Type>
class DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>
        : public DRefDetails<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>
{
public:
    using Attributes = DRefAttributes<ConstAPI, ConstRef, NotNullable>;
    using Details = DRefDetails<T_Type, Attributes>;
    using OwnType = DRef<T_Type, Attributes>;

    // (1)
    DRef() = delete;//NB: Forbidden because of NotNullable

    // (2)
    template<class _T_Type, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DOwningRef<_T_Type>& inRef);

    DRef(const OwnType& inRef);
    DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef);
    template<class _T_Type, class T_APIMut, Where(isAAssignableFromB<T_Type*, _T_Type*>())>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef);
#if !DRef_AllowImplicitRValueToLValueCast
    template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability>
    DRef(DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>&& inRef) = delete;
#endif

    // (3) NB: Forbidden at all because of ConstRef
    OwnType& operator=(const OwnType& inRef) = delete;
    OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden because of NotNullable

    // (4)
    const T_Type* operator->() const;
    const T_Type& operator*() const;

    // (5,6) NB: No sense because of NotNullable

    // --- Implementation-specific
    ~DRef();
};

// =========================================================================================================================================

//template<typename T_TypeTo, typename T_TypeFrom, typename T_APIMut, typename T_RefMut, typename T_Nullability>
//DRef<T_TypeTo, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>> DDynamicCast(
//        const DRef<T_TypeFrom, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRefFrom)
//{
//    return DRef<T_TypeTo, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>(
//        dynamic_cast<T_TypeTo*>(inRefFrom._getObjectPointer()), inRefFrom._getSEUDOffset()
//    );
//}

//template<typename T_TypeTo, typename T_TypeFrom>
//DOwningRef<T_TypeTo> DDynamicCast(DOwningRef<T_TypeFrom>&& inRefFrom)
//{
//    static_assert(is_base_of<T_TypeTo, T_TypeFrom>::value || is_base_of<T_TypeFrom, T_TypeTo>::value, "Types are not related");
//    typedef typename AddressWithOtherType<T_TypeTo, decltype(inRefFrom._COP)>::ValueType CastedType;
//    CastedType theResultPointer = dynamic_cast<CastedType>(inRefFrom._COP.value);
//    DCheck(!inRefFrom.isValid() || nullptr != theResultPointer);
//    DOwningRef<T_TypeTo> theResult{ theResultPointer, inRefFrom._getSEUDOffset() };
//    inRefFrom.invalidate();
//    return theResult;
//}

//template<typename T_Type, typename T_RefMut, typename T_Nullability>
//DRef<T_Type, DRefAttributes<ConstAPI, T_RefMut, T_Nullability>> DConstCast(
//        DRef<T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>)
//{
//    //TODO: Implement casting
//    return DRef<T_Type, DRefAttributes<ConstAPI, T_RefMut, T_Nullability>>();
//}
//
//template<typename T_Type, typename T_APIMut, typename T_RefMut>
//DRef<T_Type, DRefAttributes<T_APIMut, T_RefMut, Nullable>> DNullableCast(
//        DRef<T_Type, DRefAttributes<T_APIMut, T_RefMut, NotNullable>>)
//{
//    //TODO: Implement casting
//    return DRef<T_Type, DRefAttributes<T_APIMut, T_RefMut, Nullable>>();
//}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//=====================================================[ Nullable ]=========================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M M] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::DRef()
    : Details(Fit<T_Type*>::_(nullptr), sizeof(T_Type), 0)
{
}

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::DRef(
        const DOwningRef<_T_Type>& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::DRef(
        const OwnType& inRef)
    : Details(inRef._COP, inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::DRef(
        OwnType&& inRef)
    : Details(inRef._COP, inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

template<class T_Type>
template<class _T_Type, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::DRef(
        DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator=(
        const DOwningRef<_T_Type>& inRef)
{
    Details::_assign(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID());
    return *this;
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator=(
        const OwnType& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset, inRef._SEGID);
    return *this;
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator=(
        OwnType&& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset, inRef._SEGID);
    inRef.invalidate();
    return *this;
}

template<class T_Type>
template<class _T_Type, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator=(
        const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef)
{
    Details::_assign(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID());
    return *this;
}

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator=(
        DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset, inRef._SEGID);
    inRef.invalidate();
    return *this;
}

// (4)---Dereferencing

template<class T_Type>
T_Type* DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator->() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return Details::_getCOP();
}

template<class T_Type>
T_Type& DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::operator*() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return *Details::_getCOP();
}

// (5)---Validity checking

template<class T_Type>
bool DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::isValid() const
{
    return Details::_isValid();
}

// (6)---Invalidation

template<class T_Type>
void DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>::invalidate()
{
    Details::_assign(Fit<T_Type*>::_(nullptr), sizeof(T_Type), 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::DRef()
    : Details(Fit<const T_Type*>::_(nullptr), sizeof(T_Type), 0)
{
}

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::DRef(
        const DOwningRef<_T_Type>& inRef)
    : Details(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::DRef(
        const OwnType& inRef)
    : Details(inRef._COP, inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::DRef(
        OwnType&& inRef)
    : Details(inRef._COP, inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

template<class T_Type>
template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
template<class _T_Type, class T_APIMut, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::DRef(
        DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator=(
        const DOwningRef<_T_Type>& inRef)
{
    Details::_assign(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID());
    return *this;
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator=(
        const OwnType& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset, inRef._SEGID);
    return *this;
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator=(
        OwnType&& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset, inRef._SEGID);
    inRef.invalidate();
    return *this;
}

template<class T_Type>
template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator=(
        const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef)
{
    Details::_assign(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID());
    return *this;
}

template<class T_Type>
template<class _T_Type, class T_APIMut, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator=(
        DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset, inRef._SEGID);
    inRef.invalidate();
    return *this;
}

// (4)---Dereferencing

template<class T_Type>
const T_Type* DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator->() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return Details::_getCOP();
}

template<class T_Type>
const T_Type& DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::operator*() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return *Details::_getCOP();
}

// (5)---Validity checking

template<class T_Type>
bool DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::isValid() const
{
    return Details::_isValid();
}

// (6)---Invalidation

template<class T_Type>
void DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, Nullable>>::invalidate()
{
    Details::_assign(Fit<T_Type*>::_(nullptr), sizeof(T_Type), 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::DRef()
    : Details(Fit<T_Type&>::_(nullptr), sizeof(T_Type), 0)
{
}

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::DRef(const DOwningRef<_T_Type>& inRef)
    : Details(Fit<_T_Type&>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::DRef(const OwnType& inRef)
    : Details(inRef._COP, inRef._getSEUDOffset(), inRef._getSEGID())
{
}

//DRef(OwnType&& inRef) = delete;//NB: Forbidden because of ConstRef

template<class T_Type>
template<class _T_Type, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<_T_Type&>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::DRef(
        DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)
//OwnType& operator=(const OwnType& inRef) = delete;//NB: Forbidden at all because of ConstRef
//OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden at all because of ConstRef

// (4)---Dereferencing

template<class T_Type>
T_Type* DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::operator->() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return &Details::_getCOP();
}

template<class T_Type>
T_Type& DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::operator*() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return Details::_getCOP();
}

// (5)---Validity checking

template<class T_Type>
bool DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, Nullable>>::isValid() const
{
    return Details::_isValid();
}

// (6)---Invalidation
//NB: Forbidden at all because of ConstRef

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::DRef()
    : Details(Fit<const T_Type&>::_(nullptr), sizeof(T_Type), 0)
{
}

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::DRef(const DOwningRef<_T_Type>& inRef)
    : Details(Fit<const _T_Type&>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::DRef(const OwnType& inRef)
    : Details(inRef._COP, inRef._getSEUDOffset(), inRef._getSEGID())
{
}

//DRef(OwnType&& inRef) = delete;//NB: Forbidden because of ConstRef

template<class T_Type>
template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<const _T_Type&>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
}

template<class T_Type>
template<class _T_Type, typename T_APIMut, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::DRef(DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)
//OwnType& operator=(const OwnType& inRef) = delete;//NB: Forbidden because of ConstRef
//OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden because of ConstRef

// (4)---Dereferencing

template<class T_Type>
const T_Type* DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::operator->() const
{
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return &Details::_getCOP();
}

template<class T_Type>
const T_Type& DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::operator*() const {
    DCheck(isValid(), Check::DereferencingOfInvalidObjectException{ });
    return Details::_getCOP();
}

// (5)---Validity checking

template<class T_Type>
bool DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, Nullable>>::isValid() const
{
    return Details::_isValid();
}

// (6)---Invalidation
//NB: Forbidden at all because of ConstRef


//===================================================[ Not Nullable ]=======================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing
//DRef() = delete;//NB: Forbidden because of NotNullable

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::DRef(
        const DOwningRef<_T_Type>& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::DRef(
        const OwnType& inRef)
    : Details(inRef._COP, inRef._SEUDOffset)
{
}

//DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

template<class T_Type>
template<class _T_Type, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset())
{
}

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::DRef(
        DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::operator=(
        const DOwningRef<_T_Type>& inRef)
{
    DCheck(isRefValid(inRef), Check::SetNullForNotNullableRefException{ });
    Details::_assign(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset());
    return *this;
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::operator=(
        const OwnType& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset);
    return *this;
}

template<class T_Type>
template<class _T_Type, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::operator=(
        const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef)
{
    DCheck(isRefValid(inRef), Check::SetNullForNotNullableRefException{ });
    Details::_assign(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset());
    return *this;
}

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::operator=(
        DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef)
{
    DCheck(isRefValid(inRef), Check::SetNullForNotNullableRefException{ });
    Details::_assign(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset());
    inRef.invalidate();
    return *this;
}

// (4)---Dereferencing

template<class T_Type>
T_Type* DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::operator->() const
{
    return Details::_getCOP();
}

template<class T_Type>
T_Type& DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::operator*() const
{
    return *Details::_getCOP();
}

// (5,6)---Validity checking & Invalidation
//NB: No sense because of NotNullable

// --- Implementation-specific

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, MutableRef, NotNullable>>::~DRef()
{
    unregisterReferenceForObjectDestroyChecks(this);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C M]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing
//DRef() = delete;//NB: Forbidden because of NotNullable

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::DRef(const DOwningRef<_T_Type>& inRef)
    : Details(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::DRef(const OwnType& inRef)
    : Details(inRef._COP, inRef._SEUDOffset)
{
}

//DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

template<class T_Type>
template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset())
{
}

template<class T_Type>
template<class _T_Type, class T_APIMut, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::DRef(
        DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::operator=(const DOwningRef<_T_Type>& inRef)
{
    DCheck(isRefValid(inRef), Check::SetNullForNotNullableRefException{ });
    Details::_assign(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset());
    return *this;
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::operator=(const OwnType& inRef)
{
    Details::_assign(inRef._COP, inRef._SEUDOffset);
    return *this;
}

template<class T_Type>
template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::operator=(
        const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef)
{
    DCheck(isRefValid(inRef), Check::SetNullForNotNullableRefException{ });
    Details::_assign(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset());
    return *this;
}

template<class T_Type>
template<class _T_Type, class T_APIMut, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>&
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::operator=(
        DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef)
{
    DCheck(isRefValid(inRef), Check::SetNullForNotNullableRefException{ });
    Details::_assign(Fit<const _T_Type*>::_(inRef._COP), inRef._getSEUDOffset());
    inRef.invalidate();
    return *this;
}


// (4)---Dereferencing

template<class T_Type>
const T_Type* DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::operator->() const
{
    return Details::_getCOP();
}

template<class T_Type>
const T_Type& DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::operator*() const
{
    return *Details::_getCOP();
}

// (5,6)---Validity checking & Invalidation
//NB: No sense because of NotNullable

// --- Implementation-specific

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, MutableRef, NotNullable>>::~DRef()
{
    unregisterReferenceForObjectDestroyChecks(this);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[M C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing
//DRef() = delete;//NB: Forbidden because of NotNullable

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::DRef(const DOwningRef<_T_Type>& inRef)
    : Details(Fit<_T_Type&>::_(inRef._getCOP()), inRef._getSEUDOffset())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::DRef(const OwnType& inRef)
    : Details(inRef._COP, inRef._SEUDOffset)
{
}

//DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

template<class T_Type>
template<class _T_Type, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<MutableAPI, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<_T_Type&>::_(inRef._getCOP()), inRef._getSEUDOffset())
{
}

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::DRef(
        DRef<_T_Type, DRefAttributes<MutableAPI, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID())
{
    inRef.invalidate();
}

// (3)---Assigning & moving (assignment)
//OwnType& operator=(const OwnType& inRef) = delete;//NB: Forbidden at all because of ConstRef
//OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden at all because of ConstRef & NotNullable

// (4)---Dereferencing

template<class T_Type>
T_Type* DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::operator->() const
{
    return &Details::_getCOP();
}

template<class T_Type>
T_Type& DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::operator*() const
{
    return Details::_getCOP();
}

// (5,6)---Validity checking & Invalidation
//NB: No sense because of NotNullable

// --- Implementation-specific

template<class T_Type>
DRef<T_Type, DRefAttributes<MutableAPI, ConstRef, NotNullable>>::~DRef()
{
    unregisterReferenceForObjectDestroyChecks(this);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - -[C C]- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// (1)---Default constructing
//DRef() = delete;//NB: Forbidden because of NotNullable

// (2)---Assigning & moving (constructing)

template<class T_Type>
template<class _T_Type, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::DRef(const DOwningRef<_T_Type>& inRef)
    : Details(Fit<const _T_Type&>::_(inRef._COP), inRef._getSEUDOffset())
{
}

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::DRef(const OwnType& inRef)
    : Details(inRef._COP, inRef._SEUDOffset)
{
}

//DRef(OwnType&&) = delete;//NB: Forbidden because of NotNullable

template<class T_Type>
template<class _T_Type, class T_APIMut, class T_RefMut, class T_Nullability, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::DRef(
        const DRef<_T_Type, DRefAttributes<T_APIMut, T_RefMut, T_Nullability>>& inRef)
    : Details(Fit<const _T_Type&>::_(inRef._COP), inRef._getSEUDOffset())
{
}

template<class T_Type>
template<class _T_Type, class T_APIMut, WhereImpl(isAAssignableFromB<T_Type*, _T_Type*>())>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::DRef(
        DRef<_T_Type, DRefAttributes<T_APIMut, MutableRef, Nullable>>&& inRef)
    : Details(Fit<_T_Type*>::_(inRef._COP), inRef._getSEUDOffset(), inRef._getSEGID()) { inRef.invalidate(); }

// (3)---Assigning & moving (assignment)
//OwnType& operator=(const OwnType& inRef) = delete;//NB: Forbidden at all because of ConstRef
//OwnType& operator=(OwnType&& inRef) = delete;//NB: Forbidden at all because of ConstRef & NotNullable

// (4)---Dereferencing

template<class T_Type>
const T_Type* DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::operator->() const
{
    return &Details::_getCOP();
}

template<class T_Type>
const T_Type& DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::operator*() const
{
    return Details::_getCOP();
}

// (5,6)---Validity checking & Invalidation
//NB: No sense because of NotNullable

// --- Implementation-specific

template<class T_Type>
DRef<T_Type, DRefAttributes<ConstAPI, ConstRef, NotNullable>>::~DRef()
{
    unregisterReferenceForObjectDestroyChecks(this);
}

#endif
