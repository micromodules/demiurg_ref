#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_ref__Version

#include "DOwningRef.hpp"

template<typename T_Type>
struct DStorageHolder {
public:
    DOwningRef<T_Type> create() {
        return {storage.create(), sizeof(T_Type)};
    }

private:
    DStorageBase storage{ sizeof(T_Type) };
};

template<typename T_Type>
DOwningRef<T_Type> DNew() {    
    static DStorageHolder<T_Type> storageHolder;
    return storageHolder.create();
}

#endif
